const Course = require('../models/Course');


//Create a new course
/*
Steps:
	1. Create ane Course object using the mongoose model and the information from the request body.
	2. Save the new User to the databse
*/
module.exports.addCourse = (reqBody) => {

	//create a variable "newCourse" and instantiatethe name, description, price
	let newCourse = new Course({
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price	
	});

	//Save the created object to our databse
	return newCourse.save().then((course, error) => {
		//Course creation is successful or not
		if (error) {
			return false;
		} else {
			return true;
		}
	}).catch(error => error)
}


//Retrieve all courses

//1. Retrive all the couress from the database

module.exports.getAllCourses = () => {
	return Course.find({}).then(result => {
		return result;
	}).catch(error => error)
}


//Retrive all ACTIVE courses
//1. Retrieve all the courses with the property isActive: true

module.exports.getAllActive = () => {
	return Course.find({ isActive: true }).then(result => {
		return result
	}).catch(error => error)
}



//Retrieving a specific courses
//1. Retrieve the course that matches the course ID provided from the URL

module.exports.getCourse = (reqParams) => {
	return Course.findById(reqParams).then(result => {
		return result
	}).catch(error => error)
}


//UPDATE	a course
/*
STEPS:
	1. Create a variable 'updatedCourse' which will conatin the info rertrived from the req.body
	2. Find and Update the course using the courseID retrived from the req.params and the variable "updatedCourse" containing info from req.body

*/

module.exports.updatedCourse = (courseId, data) => {
	//specify the fields/properties of the document to be updated
	let updatedCourse = {
		name: data.name,
		description: data.description,
		price: data.price
	}

	//findByIdAndUpdate(document id, updatesToBeApplied)
	return Course.findByIdAndUpdate(courseId, updatedCourse).then((course, error) => {
		if (error) {
			return false;
		} else {
			return true;
		}
	}).catch(error => error)
}


//Archiving a cpurse
//1.Update the status of 'isActive' into "false" which will no longer be displayed in the client whenever all active courses are retrieved.
module.exports.archiveCourse = (courseId) => {
	let updateActiveField = {
		isActive: false
	};

	return Course.findByIdAndUpdate(courseId, updateActiveField).then ((course, error) => {
		if (error) {
			return false;
		} else {
			return true;
		}
	}).catch(error => error)
}
